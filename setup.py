from setuptools import setup

setup(
    name='Anastasia',
    version='0.0.1',
    description='Music indexing daemon',
    author='Matthew Summers',
    author_email='matt@wishf.co.uk',

    packages=['anastasia'],

    install_requires=[
        'colorama',
        'sqlalchemy',
        'mutagen',
        'six',
        #'pyinotify'
    ],

    entry_points={
        'console_scripts': [
            'anastasia=anastasia.scripts:tool_main'
        ],
    }
)