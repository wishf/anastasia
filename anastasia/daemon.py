from anastasia.config import Config


class Daemon:
    def __init__(self, config_path=None):
        self.config = Config(path=config_path)
        self.session = self.config.session()

    def run(self):
        pass
