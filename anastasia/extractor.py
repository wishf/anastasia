import os
import re
import mutagen
import six


class Extractor:
    _FlacOggTagDescriptor = {
        'title': ('TITLE',),
        'album': ('ALBUM',),
        'artist': ('ARTIST',),
        'album_artist': ('ALBUMARTIST',),
        'track_number': ('TRACKNUMBER',),
        'disc_number': ('DISC_NUMBER',),
        'genre': ('GENRE',),
    }

    _IDv3TagDescriptor = {
        'title': ('TIT2',),
        'album': ('TALB',),
        'artist': ('TPE1', 'TPE2'),
        'album_artist': ('TPE2', 'TXXX:ALBUM ARTIST'),
        'track_number': ('TRCK',),
        'disc_number': ('TPOS',),
        'genre': ('TCON',),
    }

    _APEv2TagDescriptor = {
        'title': ('Title',),
        'album': ('Album',),
        'artist': ('Artist',),
        'album_artist': ('Album Artist',),
        'track_number': ('Track',),
        'disc_number': ('Disc',),
        'genre': ('Genre',),
    }

    _iTunesCTagDescriptor = {
        'title': ('©nam',),
        'album': ('©alb',),
        'artist': ('©art',),
        'album_artist': ('aArt',),
        'track_number': ('trkn',),
        'disc_number': ('disk',),
        'genre': ('©gen',),
    }

    image_test = re.compile('(png|jpg|jpeg|gif)$')

    search_paths = ['.', '..',
                    '../scans',
                    '../scan',
                    '../bk',
                    './bk',
                    './scan',
                    './scans']

    @staticmethod
    def load_tags(file, ext):
        if ext == 'flac' or ext == 'ogg':
            descriptors = [Extractor._FlacOggTagDescriptor]
        elif ext == 'mp3':
            descriptors = [Extractor._IDv3TagDescriptor]
        elif ext == 'aac':
            descriptors = [Extractor._IDv3TagDescriptor, Extractor._iTunesCTagDescriptor]
        else:
            # TODO: Specialise
            raise BaseException

        dic = Extractor.__load_tags(file, descriptors)

        if 'artist' not in dic:
            dic['artist'] = 'Unknown'

        if 'title' not in dic:
            dic['title'] = '[SYS]Unknown by {0}'.format(dic['artist'])

        if 'disc_number' not in dic:
            dic['disc_number'] = 1

        if 'album_artist' not in dic:
            dic['album_artist'] = dic['artist']

        if 'track_number' not in dic:
            dic['track_number'] = 1

        if 'album' not in dic:
            dic['album'] = dic['title']

        if 'genre' not in dic:
            dic['genre'] = 'Unclassified'

        if isinstance(dic['track_number'], six.string_types):
            dic['track_number'] = int(dic['track_number'].split('/')[0])

        if isinstance(dic['disc_number'], six.string_types):
            dic['disc_number'] = int(dic['disc_number'].split('/')[0])

        return dic

    @staticmethod
    def __load_tags(file, descriptors):
        data = {}

        for descriptor in descriptors:
            for block_label, block in descriptor.items():
                if block_label not in data:
                    tag = Extractor.__load_tag(file, block)

                    if tag:
                        data[block_label] = tag

        return data

    @staticmethod
    def __load_tag(file, descriptor_block):
        for tag in descriptor_block:
            try:
                return file[tag][0]
            except KeyError:
                pass

        return None

    @staticmethod
    def check_art(file, ext):
        if ext == 'mp3' or ext == 'aac':
            return file.tags and 'APIC:' in file.tags
        elif ext == 'flac':
            if file.pictures:
                return True
            else:
                return False
        elif ext == 'ogg':
            return 'METADATA_BLOCK_PICTURE' in file.tags
        else:
            # TODO: Specialise
            raise BaseException

    @staticmethod
    def find_art_directories(dir):
        dirs = []
        for search_offset in Extractor.search_paths:
            search_location = os.path.normpath(os.path.join(dir, search_offset))

            if os.path.exists(search_location):
                for path in os.listdir(search_location):
                    if Extractor.image_test.search(path):
                        dirs.append(search_location)
                        break

        return dirs

    @staticmethod
    def extract(dir, file):
        ext = os.path.splitext(file)[1][1:].lower()
        track = mutagen.File(os.path.join(dir, file))
        metadata = {
            'tags': Extractor.load_tags(track, ext),
            'length': track.info.length,
            'embedded_art': Extractor.check_art(track, ext)
        }

        if not metadata['embedded_art']:
            metadata['art_directories'] = Extractor.find_art_directories(dir)

        return metadata
