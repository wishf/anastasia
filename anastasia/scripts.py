import argparse
import os
from anastasia.daemon import Daemon
from anastasia.scanner import Scanner
from anastasia.setup import Setup


def tool_main():
    parser = argparse.ArgumentParser()

    mode = parser.add_mutually_exclusive_group(required=True)

    # Run in scanner mode
    mode.add_argument('-s', '--scanner',
                        dest='mode', action='store_const',
                        const=lambda x: Scanner(config_path=x),
                        help='Scans the configured directory for tracks and puts them in the DB')

    # Run in daemon mode
    mode.add_argument('-d', '--daemonise',
                      dest='mode', action='store_const',
                      const=lambda x: Daemon(config_path=x),
                      help='Starts a daemon to watch a directory and track updates in the DB')

    # Run in setup mode
    mode.add_argument('-e', '--setup',
                      dest='mode', action='store_const',
                      const=lambda x: Setup(save_path=x),
                      help='Enters an interactive configuration mode')

    # Set config file
    parser.add_argument('-c', '--config', default='config.ini')

    args = parser.parse_args()

    mode_manager = args.mode(os.path.join(os.getcwd(), args.config))
    mode_manager.run()
