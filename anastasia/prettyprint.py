from colorama import init, deinit, Fore


class PrettyPrinter:
    def __init__(self):
        # Enable colorama
        init()

        # Initialise categories dictionary
        self.categories = {}

    def __del__(self):
        deinit()

    def register_category(self, category, colour):
        # Throw exception if category already exists
        if category in self.categories:
            raise KeyError

        self.categories[category] = colour

    def print(self, message, category):
        print(self.categories[category] + '[{0}] '.format(category) + Fore.WHITE + message)

    @staticmethod
    def default():
        printer = PrettyPrinter()

        printer.register_category('info', Fore.CYAN)
        printer.register_category('error', Fore.RED)
        printer.register_category('warn', Fore.YELLOW)

        return printer