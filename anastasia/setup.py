from anastasia.config import Config


class Setup:
    def __init__(self, save_path='config.ini'):
        self.save_path = save_path
        self.config = Config()

    def run(self):
        print("Set base directory")
        self.config.base_dir = input('> ')

        print("Set database url")
        self.config.db_string = input('> ')

        self.config.save(path=self.save_path)