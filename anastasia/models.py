from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, Float
from sqlalchemy.orm import relationship, backref


Base = declarative_base()


class Album(Base):
    __tablename__ = 'albums'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    is_art_in_track = Column(Boolean)

    artist_id = Column(Integer, ForeignKey('artists.id'))
    artist = relationship("Artist", backref=backref('albums', lazy='dynamic'))

    def __repr__(self):
        return "<Album(name='{0}', artist='{1}, embedded_art='{2}'>".format(
            self.name,
            self.artist,
            self.is_art_in_track
        )

    def __init__(self, name, artist, art_in_track):
        self.name = name
        self.artist = artist
        self.is_art_in_track = art_in_track

    def add_art_directory(self, dir):
        exists = self.album_art_directories.filter_by(directory=dir).first()

        if exists:
            return

        art_dir = AlbumArtDirectory(dir)
        self.album_art_directories.append(art_dir)
        return art_dir


class AlbumArtDirectory(Base):
    __tablename__ = 'art_dirs'

    id = Column(Integer, primary_key=True)
    directory = Column(String)

    album_id = Column(Integer, ForeignKey('albums.id'))
    album = relationship("Album", backref=backref('album_art_directories', lazy='dynamic'))

    def __repr__(self):
        return "<AlbumArtDirectory(directory='{0}')>".format(self.directory)

    def __init__(self, directory):
        self.directory = directory


class Track(Base):
    __tablename__ = 'tracks'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    length = Column(Float)
    path = Column(String)
    number = Column(Integer)
    disc = Column(Integer)
    embedded_art = Column(Boolean)

    album_id = Column(Integer, ForeignKey('albums.id'))
    album = relationship("Album", backref=backref('tracks', lazy='dynamic', order_by=number))

    artist_id = Column(Integer, ForeignKey('artists.id'))
    artist = relationship("Artist", backref=backref('tracks', lazy='dynamic'))

    genre_id = Column(Integer, ForeignKey('genres.id'))
    genre = relationship("Genre", backref=backref('tracks', lazy='dynamic'))

    def __repr__(self):
        return ("<Track("
                "name='{0}, artist='{1}', length='{2}', number={3}, disc={4}, path='{5}', album='{6}', art='{7}'"
                ")>").format(
            self.name,
            self.artist,
            self.length,
            self.number,
            self.disc,
            self.path,
            self.album.name,
            self.embedded_art
        )

    def __init__(self, number, name, length, artist, path, disc, embedded_art, album, genre):
        self.name = name
        self.artist = artist
        self.path = path
        self.length = length
        self.disc = disc
        self.number = number
        self.embedded_art = embedded_art
        self.album = album
        self.genre = genre


class Artist(Base):
    __tablename__ = 'artists'

    id = Column(Integer, primary_key=True)

    def __repr__(self):
        return "<Artist(aliases={0})>".format(self.aliases.count())

    def __init__(self):
        pass

class Alias(Base):
    __tablename__ = 'aliases'

    id = Column(Integer, primary_key=True)

    name = Column(String)

    artist_id = Column(Integer, ForeignKey('artists.id'))
    artist = relationship("Artist", backref=backref('aliases', lazy='dynamic'))

    def __repr__(self):
        return "<Alias(name='{0}')>".format(self.name)

    def __init__(self, name, artist):
        self.name = name
        self.artist = artist


class Genre(Base):
    __tablename__ = 'genres'

    id = Column(Integer, primary_key=True)

    name = Column(String)

    def __repr__(self):
        return "<Genre(name='{0}')>".format(self.name)

    def __init__(self, name):
        self.name = name