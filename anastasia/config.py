import configparser
import sqlalchemy
from sqlalchemy.orm import sessionmaker
import anastasia.models as models


class Config:

    @property
    def base_dir(self):
        return self.__base_dir

    @base_dir.setter
    def base_dir(self, value):
        self.__base_dir = value

    @property
    def db_string(self):
        return self.__db_string

    @db_string.setter
    def db_string(self, value):
        self.__db_string = value

        self.__db_setup()

    def __init__(self, path=None):
        self.__db_string = None
        self.__base_dir = None

        self.__sessionmaker = sessionmaker()

        if path:
            self.path = path
            config = configparser.ConfigParser()
            config.read(path)

            if 'Database' not in config.sections() \
               and 'Indexer' not in config.sections():
                # TODO: Specialise
                raise BaseException

            self.base_dir = config['Indexer']['base-dir']
            self.db_string = config['Database']['connection-string']

            self.__db_setup()

    def save(self, path=None):
        if not path:
            path = self.path

        if path:
            config = configparser.ConfigParser()
            config['Database'] = {'connection-string': self.db_string}
            config['Indexer'] = {'base-dir': self.base_dir}

            with open(path, 'w') as f:
                config.write(f)
        else:
            # TODO: Specialise
            raise BaseException

    def __db_setup(self):
        self.__engine = sqlalchemy.create_engine(self.__db_string)
        self.__sessionmaker.configure(bind=self.__engine)

    def session(self):
        models.Base.metadata.create_all(self.__engine)

        return self.__sessionmaker()
