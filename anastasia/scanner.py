import os
import os.path
import re
import anastasia.models as models
from anastasia.config import Config
from anastasia.prettyprint import PrettyPrinter
from anastasia.extractor import Extractor
import mutagen


class Scanner:
    matcher = re.compile('(flac|aac|mp3|ogg)$')

    def __init__(self, config_path=None):
        self.config = Config(path=config_path)
        self.session = self.config.session()
        self.printer = PrettyPrinter.default()

    def run(self):
        self.printer.print('Scanning in \'{0}\''.format(self.config.base_dir), 'info')

        for dir, subs, files in os.walk(self.config.base_dir):
            for file in files:
                if self.matcher.search(file):
                    metadata = Extractor.extract(dir, file)
                    tags = metadata['tags']

                    # Construct artist and album artist
                    artist_alias = self.session.query(models.Alias).filter_by(name=tags['artist']).first()

                    if not artist_alias:
                        artist = models.Artist()
                        artist_alias = models.Alias(tags['artist'], artist)
                        self.session.add(artist_alias)
                        self.session.add(artist)
                    else:
                        artist = artist_alias.artist

                    print("Artist: {0}".format(artist))
                    print("- Alias: {0}".format(artist_alias))

                    album_artist_alias = self.session.query(models.Alias).filter_by(name=tags['album_artist']).first()

                    if not album_artist_alias:
                        album_artist = models.Artist()
                        album_artist_alias = models.Alias(tags['album_artist'], album_artist)
                        self.session.add(album_artist_alias)
                        self.session.add(album_artist)
                    else:
                        album_artist = album_artist_alias.artist

                    print("Album Artist: {0}".format(album_artist))
                    print("- Alias: {0}".format(album_artist_alias))

                    # Construct album
                    album = self.session.query(models.Album).filter_by(name=tags['album']).first()

                    if not album:
                        album = models.Album(tags['album'], album_artist, metadata['embedded_art'])

                    if not album.is_art_in_track and metadata['embedded_art']:
                        album.is_art_in_track = True

                    if not metadata['embedded_art']:
                        for art_dir in metadata['art_directories']:
                            entry = album.add_art_directory(art_dir)
                            print(entry)

                            if entry:
                                self.session.add(entry)

                    print("Album: {0}".format(album))

                    # Construct genre
                    genre = self.session.query(models.Genre).filter_by(name=tags['genre']).first()

                    if not genre:
                        genre = models.Genre(tags['genre'])
                        self.session.add(genre)

                    print("Genre: {0}".format(genre))

                    # Construct track
                    track = models.Track(
                        tags['track_number'],
                        tags['title'],
                        metadata['length'],
                        artist,
                        os.path.join(dir, file),
                        tags['disc_number'],
                        metadata['embedded_art'],
                        album,
                        genre)
                    self.session.add(track)

                    print("Track: {0}".format(track))

                    self.session.commit()

